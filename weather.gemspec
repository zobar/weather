require_relative 'lib/weather/version'

Gem::Specification.new do |gem|
  gem.author      = 'David P Kleinschmidt'
  gem.description = 'Periodically tweets weather predictions.'
  gem.email       = 'david@kleinschmidt.name'
  gem.homepage    = 'http://bitbucket.com/zobar/weather'
  gem.license     = 'MIT'
  gem.name        = 'weather'
  gem.summary     = 'Tweets the weather'
  gem.version     = Weather::VERSION

  gem.files       = Dir['lib/**/*.rb']
  gem.executables = ['weather']

  gem.add_dependency 'bitly'
  gem.add_dependency 'loofah'
  gem.add_dependency 'redis'
  gem.add_dependency 'rmagick'
  gem.add_dependency 'thor'
  gem.add_dependency 'twitter'
end
