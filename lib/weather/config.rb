module Weather
  class Config
    attr_reader :env

    def initialize(env)
      @env = env
    end

    def bitly_api_key
      @bitly_api_key ||= get 'BITLY_API_KEY'
    end

    def latitude
      @latitude ||= get 'LATITUDE'
    end

    def longitude
      @longitude ||= get 'LONGITUDE'
    end

    def redistogo_url
      @redistogo_url ||= get 'REDISTOGO_URL'
    end

    def twitter_access_token
      @twitter_access_token ||= get 'TWITTER_ACCESS_TOKEN'
    end

    def twitter_access_token_secret
      @twitter_access_token_secret ||= get 'TWITTER_ACCESS_TOKEN_SECRET'
    end

    def twitter_consumer_key
      @twitter_consumer_key ||= get 'TWITTER_CONSUMER_KEY'
    end

    def twitter_consumer_secret
      @twitter_consumer_secret ||= get 'TWITTER_CONSUMER_SECRET'
    end

    def wunderground_api_key
      @wunderground_api_key ||= get 'WUNDERGROUND_API_KEY'
    end

    private

    def get(key)
      raise "No #{key} in environment" unless env.has_key? key
      env[key]
    end
  end
end
