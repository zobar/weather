require 'redis'

module Weather
  class Storage
    class << self
      def with_config(config)
        new Redis.new(url: config.redistogo_url)
      end
    end

    attr_reader :redis

    def cache(key)
      cached = redis.get key
      unless cached
        cached = yield
        redis.setnx key, cached
      end
      cached
    end

    def initialize(redis)
      @redis = redis
    end

    def if_changed(key, value)
      yield if redis.getset(key, value) != value
    end
  end
end
