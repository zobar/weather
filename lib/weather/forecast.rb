require 'loofah'
require 'pp'
require 'weather/underground'

module Weather
  class Forecast
    class << self
      def with_config(config)
        new Underground.with_config(config), config.latitude, config.longitude
      end
    end

    attr_reader :latitude, :longitude, :wunderground

    def icon
      @icon ||= summary['icon']
    end

    def icon_url
      @icon_url ||= "http:#{summary['icon_url']}"
    end

    def initialize(wunderground, latitude, longitude)
      @wunderground = wunderground
      @latitude = latitude
      @longitude = longitude
    end

    def link
      @link ||= "https://wunderground.com/cgi-bin/findweather/getForecast?query=#{location}"
    end

    def quickie
      @quickie ||= Loofah.fragment(quickie_html).to_text
    end

    private

    def location
      @location ||= "#{latitude},#{longitude}"
    end

    def quickie_html
      @quickie_html ||= summary['weather_quickie']
    end

    def response
      @response ||= wunderground.get(location, :forecast, v: '2.0')
    end

    def summary
      @summary ||= response['forecast']['days'].first['summary']
    end
  end
end
