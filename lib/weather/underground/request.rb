require 'json'
require 'net/http'

module Weather
  class Underground
    class Request
      attr_reader :api_key, :host, :options, :query

      def initialize(host, api_key, query, *options)
        @api_key = api_key
        @host = host
        @options = options
        @query = query
      end

      def response
        @response ||= JSON.parse Net::HTTP.get(host, path)
      end

      private

      def host
        'api.wunderground.com'
      end

      def path
        @path ||= (
            ['', 'api', api_key] +
            options.map(&method(:path_component)) +
            ['q', query]).
          join('/') + '.json'
      end

      def path_component(option)
        if option.is_a? Enumerable
          option.map { |kv| kv.join ':' }.join '/'
        else
          option
        end
      end
    end
  end
end
