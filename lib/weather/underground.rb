module Weather
  class Underground
    autoload :Request, 'weather/underground/request'

    class << self
      def with_config(config)
        new config.wunderground_api_key
      end
    end

    attr_reader :api_key

    def initialize(api_key)
      @api_key = api_key
    end

    def get(*args)
      Request.new(host, api_key, *args).response
    end

    private

    def host
      'api.wunderground.com'
    end
  end
end
