require 'pp'
require 'thor'
require 'weather/config'
require 'weather/forecast'
require 'weather/notification'
require 'weather/shortener'
require 'weather/storage'
require 'weather/thumbnail'

module Weather
  class Cli < Thor
    desc 'poll', 'Poll Wunderground, update Redis, and post a tweet.'
    def poll
      storage.if_changed 'icon', icon do
        notification.set_icon icon_data
      end

      storage.if_changed 'quickie', quickie do
        notification.notify status,
          display_coordinates: true,
          lat:                 config.latitude,
          long:                config.longitude
      end
    end

    private

    def config
      @config ||= Config.new ENV
    end

    def forecast
      @forecast ||= Forecast.with_config config
    end

    def icon
      @icon ||= forecast.icon
    end

    def icon_data
      @icon_data ||= storage.cache("icon:#{icon}") { thumbnail.data }
    end

    def icon_url
      @icon_url ||= forecast.icon_url
    end

    def link
      @link ||= shortener.shorten forecast.link
    end

    def notification
      @notification ||= Notification.with_config config
    end

    def quickie
      @quickie ||= forecast.quickie
    end

    def shortener
      @shortener ||= Shortener.with_config config
    end

    def status
      @status ||= "#{quickie} #{link}"
    end

    def storage
      @storage ||= Storage.with_config config
    end

    def thumbnail
      @thumbnail ||= Thumbnail.new icon_url
    end
  end
end
