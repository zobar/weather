require 'twitter/base'
require 'twitter/rest/client'

module Weather
  class Notification
    class << self
      def with_config(config)
        new Twitter::REST::Client.new(
          access_token:        config.twitter_access_token,
          access_token_secret: config.twitter_access_token_secret,
          consumer_key:        config.twitter_consumer_key,
          consumer_secret:     config.twitter_consumer_secret)
      end
    end

    attr_reader :twitter

    def initialize(twitter)
      @twitter = twitter
    end

    def notify(status, options)
      twitter.update status, options
    end

    def set_icon(icon_data)
      twitter.update_profile_image Base64.encode64(icon_data)
    end
  end
end
