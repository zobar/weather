require 'bitly'

module Weather
  class Shortener
    class << self
      def with_config(config)
        new Bitly::V3::Client.new(config.bitly_api_key)
      end
    end

    attr_reader :bitly

    def initialize(bitly)
      @bitly = bitly
    end

    def shorten(url)
      bitly.shorten(url).short_url
    end
  end
end
