require 'net/http'
require 'rvg/rvg'
require 'uri'

module Weather
  class Thumbnail
    attr_reader :url

    def data
      @data ||= image.to_blob { |i| i.format = 'png' }
    end

    def initialize(url)
      @url = URI(url)
    end

    private

    def image
      @image ||= Magick::Image.from_blob(source_data).first
    end

    def source_data
      @source_data ||= Net::HTTP.get url
    end
  end
end
