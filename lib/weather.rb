module Weather
  autoload :Cli,          'weather/cli'
  autoload :Config,       'weather/config'
  autoload :Forecast,     'weather/forecast'
  autoload :Notification, 'weather/notification'
  autoload :Shortener     'weather/shortener'
  autoload :Storage,      'weather/storage'
  autoload :Thumbnail,    'weather/thumbnail'
  autoload :VERSION,      'weather/version'
  autoload :Underground,  'weather/underground'
end
